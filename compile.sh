#!/bin/bash
# Assembling
nasm -f elf src/$1.asm

# Linking
ld -m elf_i386 -s -o out/$1 src/$1.o
