; Pro/g/ramming Challenges 4.0 - 44 FizzBuzz in Assembly

section .data
    prompt db "Input Max Number: "
    plen equ $- prompt

    fizz db 'fizz'
    fizzLen equ $- fizz

    buzz db 'buzz'
    buzzLen equ $- buzz

    newline db 0xA, 0xD

    num db '000'

section .bss
    max resb 1

    cur resb 1

    printnum resb 1

section .text
    global _start

_print:
    mov eax, 4
    mov ebx, 1
    int 80h
    ret

_print_fizz:
    mov ecx, fizz
    mov edx, fizzLen
    call _print
    ret

_print_buzz:
    mov ecx, buzz
    mov edx, buzzLen
    call _print
    ret

_start:
    mov ecx, prompt
    mov edx, plen
    call _print

    mov eax, 3
    mov ebx, 0
    mov ecx, max
    mov edx, 1
    int 80h

    sub [max], byte 30h

; Convert the BCD to binary
    
    mov ecx, [max]
    mov al, 0
    clc

fb_loop:
    inc al

    push eax
    pushf
    or al, 30h
    popf

    mov [cur], al
    pop eax

    mov bl, 3

    push ecx
    push eax

    div bl

    cmp ah, 00
    je fb_fizz

post_fizz:
    pop eax
    push eax

    mov bl, 5
    div bl

    cmp ah, 00
    je fb_buzz

post_buzz:
    cmp [printnum], byte 00
    je fb_printnum

fb_exit:
    ; Print New Line
    mov ecx, newline
    mov edx, 2  
    call _print

    pop eax
    pop ecx
    mov [printnum], byte 00

    loop fb_loop

    jmp exit

fb_fizz:
    call _print_fizz
    mov [printnum], byte 1
    jmp post_fizz

fb_buzz:
    call _print_buzz
    mov[printnum], byte 1
    jmp post_buzz

fb_printnum:
    mov ecx, cur
    mov edx, 2
    call _print
    jmp fb_exit

exit:
   mov eax, 1
   int 80h
