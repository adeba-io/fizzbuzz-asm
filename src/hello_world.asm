section .text
    global _start   ; must be declared for the linker

_start:
    mov edx, len
    dec edx

    mov edx, len    ; message length
    mov edx, 2
    ; mov ecx, msg    ; message to write
    ;mov esi, 1
    mov ecx, msg[4]
    mov ebx, 1      ; file descriptor (stdout)
    mov eax, 4      ; system call number (sys_write)
    int 0x80        ; call kernel

    mov eax, 1      ; system call nymber (sys_exit)
    int 0x80        ; call kernel

section .data
msg db 'Hello, world!', 0xa ; string to be printed
len equ $ - msg             ; length of the string

; Trying to find out about sentinel characters on strings
; msg db 'Hello, World!', 0
