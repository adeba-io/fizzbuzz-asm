
section .data
    msg1 db "The quotient is ", 0xA, 0xD
    len1 equ $- msg1

    msg2 db "the remainder is ", 0xA, 0xD
    len2 equ $- msg2

section .bss
    quo resw 2
    rem resw 2

section .text
    global _start

_start:
    ; mov ax, '8'
    ; sub ax, '0'
    mov ax, 8

    ; mov dx, '0'
    ; sub dx, '0'
    mov dx, 0

    ; mov bl, '3'
    ; sub bl, '0'
    mov bl, 5

    div bl

    add al, '0'
    mov [quo], al

    add ah, '0'
    mov [rem], ah

    ; print
    mov eax, 4
    mov ebx, 1
    mov ecx, msg1
    mov edx, len1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, quo
    mov edx, 1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, msg2
    mov edx, len2
    int 80h
; 
    mov eax, 4
    mov ebx, 1
    mov ecx, rem
    mov edx, 1
    int 80h

exit:
    mov eax, 1
    int 80h
