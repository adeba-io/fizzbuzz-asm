; Pro/g/ramming Challenges 4.0 - 44 FizzBuzz in Assembly

section .data
    prompt db "Input Max Number: "
    plen equ $- prompt

    fizz db 'fizz'
    fizzLen equ $- fizz

    buzz db 'buzz'
    buzzLen equ $- buzz

    newline db 0xA, 0xD

    incOne db '01'

section .bss
    max resb 2
    maxbin resb 1
    current resb 2
    bPrintNum resb 1

    incStore resb 2
    bcdToBin resb 2


section .text
    global _start

_print:
    mov eax, 4
    mov ebx, 1
    int 80h
    ret

_print_fizz:
    mov ecx, fizz
    mov edx, fizzLen
    call _print
    ret

_print_buzz:
    mov ecx, buzz
    mov edx, buzzLen
    call _print
    ret

_newline:
    ; Print New Line
    mov ecx, newline
    mov edx, 2  
    call _print
    ret

_bcd_to_bin:
    mov al, [bcdToBin]
    sub al, 30h
    mov dl, 10
    mul dl

    mov dx, [bcdToBin + 1]
    sub dx, 30h

    add ax, dx
    ret

_increment:
    mov [incStore], ax
    mov ecx, 2
    mov esi, 1
    clc

_increment_loop:
    mov al, [incStore + esi]
    adc al, [incOne + esi]
    aaa
    pushf
    or al, 30h
    popf

    mov [incStore + esi], al
    dec esi
    loop _increment_loop

    mov ax, [incStore]
    ret

_start:
    mov ecx, prompt
    mov edx, plen
    call _print

    mov eax, 3
    mov ebx, 0
    mov ecx, max
    mov edx, 2
    int 80h

    mov ax, [max]
    mov [bcdToBin], ax
    call _bcd_to_bin
    mov [maxbin], ax

    ; Begin Loop

    mov ecx, [maxbin]
    mov ax, '00'
    clc
    ;jmp exit

loop_begin:
    push ecx

    call _increment
    mov [current], ax
    push eax

    mov [bcdToBin], ax
    call _bcd_to_bin

    mov bl, 3
    div bl

    cmp ah, 00
    je loop_fizz

post_fizz:
    pop eax
    push eax

    mov [bcdToBin], ax
    call _bcd_to_bin

    mov bl, 5
    div bl

    cmp ah, 00
    je loop_buzz

post_buzz:
    cmp [bPrintNum], byte 00
    je loop_num

loop_exit:
    call _newline

    pop eax
    pop ecx
    mov [bPrintNum], byte 00

    loop loop_begin
    jmp exit

loop_fizz:
    call _print_fizz
    mov [bPrintNum], byte 1
    jmp post_fizz

loop_buzz:
    call _print_buzz
    mov [bPrintNum], byte 1
    jmp post_buzz

loop_num:
    mov ecx, current
    mov edx, 2
    call _print
    jmp loop_exit

exit:
    mov eax, 1
    int 80h
