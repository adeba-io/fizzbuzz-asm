; Checks if the input is 3, if so prints 'Fizz'

section .data
    prompt1 db "Input a number", 0xA, 0xD
    len1 equ $- prompt1

    fizzmsg db "Fizz", 0xA, 0xD
    fizzl equ $- fizzmsg

section .bss
    input resb 2

    output resw 1

section .text
    global _start

_start:
    ; Prompt
    mov ecx, prompt1
    mov edx, len1
    mov eax, 4
    mov ebx, 1
    int 80h

    ; Read input
    mov eax, 3
    mov ebx, 2
    mov ecx, input
    mov edx, 3
    int 80h

    mov al, [input]
    sub al, '0'

    mov bl, 3

    div bl

    cmp ah, 00
    je fizz

    mov ecx, input
    mov edx, 1
    mov eax, 4
    mov ebx, 2
    int 80h
    
    jmp exit


fizz:
    mov ecx, fizzmsg
    mov edx, fizzl
    mov eax, 4
    mov ebx, 1
    int 80h


exit:
    mov eax, 1
    int 80h
