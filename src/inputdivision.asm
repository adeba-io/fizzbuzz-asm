section .data
    prompt1 db "Input the dividend", 0xA, 0xD
    lp1 equ $- prompt1

    prompt2 db "Input the divisor", 0xA, 0xD
    lp2 equ $- prompt2

    msg1 db "The quotient is ", 0xA, 0xD
    len1 equ $- msg1

    msg2 db "the remainder is ", 0xA, 0xD
    len2 equ $- msg2

section .bss
    divnd resb 2
    divsr resb 1

    temp resw 1

    quo resw 1
    rem resw 1

section .text
    global _start

_start:
    ; Prompt for the dividend
    mov ecx, prompt1
    mov edx, lp1
    mov eax, 4
    mov ebx, 1
    int 80h
    
    ;Read and store the user input
    mov eax, 3
    mov ebx, 2
    mov ecx, divnd  
    mov edx, 3          ;5 bytes (numeric, 1 for sign) of that information
    int 80h
    
    ; Prompt for the divisor
    mov ecx, prompt2
    mov edx, lp2
    mov eax, 4
    mov ebx, 1
    int 80h
    
    ;Read and store the user input
    mov eax, 3
    mov ebx, 2
    mov ecx, divsr
    mov edx, 2          ;5 bytes (numeric, 1 for sign) of that information
    int 80h

    mov al, [divnd]
    sub al, '0'

    ; mov [temp], ax
; 
    ; mov eax, 4
    ; mov ebx, 1
    ; mov ecx, temp
    ; mov edx, 2
    ; int 80h

    ; mov bl, '3'
    ; sub bl, '0'
    mov bl, [divsr]
    sub bl, '0'

    div bl

    add al, '0'
    mov [quo], al

    add ah, '0'
    mov [rem], ah

    ; print
    mov eax, 4
    mov ebx, 1
    mov ecx, msg1
    mov edx, len1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, quo
    mov edx, 1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, msg2
    mov edx, len2
    int 80h
; 
    mov eax, 4
    mov ebx, 1
    mov ecx, rem
    mov edx, 1
    int 80h

exit:
    mov eax, 1
    int 80h
