segment .data
    pmt1 db "Enter a number", 0xA, 0xD
    pln1 equ $- pmt1

    pmt2 db "Enter another number", 0xA, 0xD
    pln2 equ $- pmt2

    msg1 db "The sum is: ", 0xA, 0xD
    mln1 equ $- msg1

segment .bss
    num1 resb 3
    num2 resb 3
    sum resb 3

section .text
    global _start

_start:

    mov ebx, 1
    mov eax, 4
    mov edx, pln1
    mov ecx, pmt1
    int 80h

    ; Read input
    mov ebx, 0
    mov eax, 3
    mov ecx, num1
    mov edx, 4
    int 80h

    mov ebx, 1
    mov eax, 4
    mov edx, pln2
    mov ecx, pmt2
    int 80h

    mov ebx, 0
    mov eax, 3
    mov ecx, num2
    mov edx, 4
    int 80h

    mov esi, 2 ; pointing to rightmost digit
    mov ecx, 3 ; num of digits
    clc ; Clear carry flag

add_loop:

    mov al, [num1 + esi] ; Gets the address of num1 + an offset (like an array)
    adc al, [num2 + esi] ; Add and carry
    aaa                     ; Adjust After Addition for Unpacked BCD

    pushf                   ; Push flags onto the stack as or sets them
    or al, 30h              ; Converts it to the right ASCii char
    popf                    ; Pop the flags off the stack

    mov [sum + esi], al     ; Add it to the sum

    dec esi
    loop add_loop

    mov ebx, 1
    mov eax, 4
    mov ecx, msg1
    mov edx, mln1
    int 80h

    mov ebx, 1
    mov eax, 4
    mov ecx, sum
    mov edx, 3
    int 80h


exit:
    mov eax, 1
    int 80h
